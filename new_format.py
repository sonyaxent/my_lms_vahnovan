

def new_format(string):
    result = ""
    for i, digit in enumerate(reversed(str(string))):
        if i != 0 and (i % 3) == 0:
            result += "."
        result += digit
    return result[::-1]


def new_format_v2(string):
  num = int(string)
  if num < 1000:
    return str(num)
  else:
    num = f'{num:,}'.replace(',','.')
    return str(num)


assert new_format("1000000") == "1.000.000"
assert (new_format("100") == "100")
assert (new_format("1000") == "1.000")
assert (new_format("100000") == "100.000")
assert (new_format("10000") == "10.000")
assert (new_format("0") == "0")
print("Success")

assert new_format_v2("1000000") == "1.000.000"
assert (new_format_v2("100") == "100")
assert (new_format_v2("1000") == "1.000")
assert (new_format_v2("100000") == "100.000")
assert (new_format_v2("10000") == "10.000")
assert (new_format_v2("0") == "0")
print("Success")