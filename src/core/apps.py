from django.apps import AppConfig


class PollsStudentsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "core"
