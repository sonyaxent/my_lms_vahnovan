from django.core.management.base import BaseCommand
from faker import Faker
from datetime import datetime


class Command(BaseCommand):
    help = "Student generator"

    def add_arguments(self, parser):
        parser.add_argument("qty", type=int, help="The amount of students to generate")

    def handle(self, *args, **kwargs):
        qty = kwargs["qty"]
        faker = Faker("EN")

        for _ in range(qty):
            student_gen = f"{faker.name()}, {faker.email()}, {faker.password()}, {faker.address()}," \
                          f" {faker.date_between_dates(date_start=datetime(1985, 1, 1))}"
            self.stdout.write(student_gen)
