from django.contrib import admin  # NOQA

from .models import Group

# Register your models here.
admin.site.register(Group)
