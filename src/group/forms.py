from django.core.exceptions import ValidationError
from django.forms import ModelForm

from group.models import Group


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ("name", "date_of_creation", "rating", "is_active")

    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    def clean_email(self):
        email = self.cleaned_data["email"]

        if "@yandex" in email.lower():
            raise ValidationError("Yandex domain is forbidden in our country")

        return email

    def clean_name(self):
        return self.normalize_text(self.cleaned_data["name"])
