from django.db import models
from faker import Faker
import random


class Group(models.Model):
    name = models.CharField(max_length=100, null=True)
    date_of_creation = models.DateField(null=True)
    rating = models.IntegerField(default=5, null=True)
    group_members_list = models.TextField(max_length=400)
    is_active = models.BooleanField(null=True)

    def __str__(self):
        return f"{self.pk} {self.name}"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()

        for _ in range(count):

            cls.objects.create(
                name=faker.user_name(),
                date_of_creation=faker.date_time_between(start_date="-10y", end_date="-5y"),
                rating=random.randint(0, 100),
                group_members_list=[faker.first_name() for _ in range(0, 30)],
                is_active=faker.pybool(),
            )
