from django.urls import path
from group.views import get_all_groups, create_group, update_group, delete_group

app_name = "group"

urlpatterns = [
    path("", get_all_groups, name="all_groups"),
    path("create_group/", create_group, name="create_group"),
    path("update/<int:pk>/", update_group, name="update_group"),
    path("delete/<int:pk>/", delete_group, name="delete_group"),
]
