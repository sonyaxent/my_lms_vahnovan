from django.http import HttpResponseRedirect
from django.db.models import Q
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from webargs.djangoparser import use_kwargs
from webargs import fields

from .forms import GroupForm
from .models import Group



def index(request):
    return render(
        request, template_name="index.html",
        context={
            "key": ["a", "B", "c"],
        },
    )


@use_kwargs(
    {
        "first_name": fields.Str(missing=None),
        "last_name": fields.Str(missing=None),
        "search_text": fields.Str(missing=None),
    },
    location="query",
)
def get_all_groups(request, **kwargs):


    groups = Group.objects.all()
    search_fields = ["name", "rating"]

    for parameter_name, parameter_value in kwargs.items():
        if parameter_value:
            if parameter_name == "search_text":
                or_filter = Q()
                for field in search_fields:
                    or_filter |= Q(**{f"{field}__icontains": parameter_value})
                groups = groups.filter(or_filter)
            else:
                groups = Group.objects.filter(**{parameter_name: parameter_value})


    return render(request, template_name="groups_list.html", context={"group": groups})


def create_group(request):
    if request.method == "POST":
        form = GroupForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("group:all_groups"))
    else:
        form = GroupForm()

    return render(request, template_name="groups_create.html", context={"form": form})





def update_group(request, pk):
    group = get_object_or_404(Group.objects.all(), pk=pk)

    if request.method == "POST":
        form = GroupForm(request.POST, instance=group)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("group:all_groups"))
    else:
        form = GroupForm(instance=group)

    return render(request, template_name="groups_edit.html", context={"form": form})

def delete_group(request, pk):
    group = get_object_or_404(Group.objects.all(), pk=pk)
    group.delete()

    return HttpResponseRedirect(reverse("group:all_groups"))

def error_404(request, exception):
    return render(request, "404.html")


