from django.contrib import admin  # NOQA

from .models import Student

# Register your models here.
admin.site.register([Student])
