# Generated by Django 4.1 on 2022-08-15 14:53

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("students", "0002_alter_student_birth_date"),
    ]

    operations = [
        migrations.AlterField(
            model_name="student",
            name="birth_date",
            field=models.DateField(
                default=datetime.datetime(2022, 8, 15, 14, 53, 33, 481658), null=True
            ),
        ),
    ]
