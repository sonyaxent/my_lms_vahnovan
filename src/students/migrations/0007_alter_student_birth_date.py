# Generated by Django 4.1 on 2022-08-16 20:06

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("students", "0006_remove_student_id_student_uuid_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="student",
            name="birth_date",
            field=models.DateField(
                default=datetime.datetime(2022, 8, 16, 20, 6, 22, 124069), null=True
            ),
        ),
    ]
