# Generated by Django 4.1 on 2022-08-16 20:12

import datetime
from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ("students", "0008_alter_student_birth_date"),
    ]

    operations = [
        migrations.AlterField(
            model_name="student",
            name="birth_date",
            field=models.DateField(
                default=datetime.datetime(2022, 8, 16, 20, 12, 9, 926178), null=True
            ),
        ),
        migrations.AlterField(
            model_name="student",
            name="uuid",
            field=models.UUIDField(
                db_index=True,
                default=uuid.uuid4,
                editable=False,
                primary_key=True,
                serialize=False,
                unique=True,
            ),
        ),
    ]
