from datetime import datetime

from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker
from group.models import Group
from students.utils import first_name_validator
from uuid import uuid4

class Person(models.Model):
    first_name = models.CharField(
        max_length=100,
        null=True,
        validators=[MinLengthValidator(2), first_name_validator],
    )
    last_name = models.CharField(
        max_length=100,
        null=True,
        validators=[
            MinLengthValidator(2),
        ],
    )
    email = models.EmailField(max_length=100, null=True)
    birth_date = models.DateField(null=True, default=datetime.now())
    grade = models.PositiveSmallIntegerField(default=0, null=True)

    def age(self):
        return datetime.now().year - self.birth_date.year

    class Meta:
        abstract = True




class Student(Person):

    uuid = models.UUIDField(
        primary_key=True, editable=False, default=uuid4, unique=True, db_index=True,
    )
    group = models.ForeignKey(to=Group, on_delete=models.CASCADE, related_name="students")
    avatar = models.ImageField(upload_to='photos/%Y/%m/%d', blank=True)
    cv = models.FileField(upload_to='cv/%Y/%m/%d', blank=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} {self.pk}"


    @classmethod
    def generate_instances(cls, count):
        faker = Faker()

        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_time_between(start_date="-30y", end_date="-18y"),
            )
