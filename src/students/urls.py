from django.urls import path

from students.views import get_all_students, create_student, update_student, delete_student

app_name = "students"

urlpatterns = [
    # path('', index, name='index'),
    path("", get_all_students, name="all_students"),
    path("create/", create_student, name="create_student"),
    path("update/<uuid:pk>/", update_student, name="update_student"),
    path("delete/<uuid:pk>/", delete_student, name="delete_student"),

]
