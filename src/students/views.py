from django.http import HttpResponseRedirect
from django.db.models import Q
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from webargs.djangoparser import use_kwargs
from webargs import fields

from .forms import StudentForm
from .models import Student



def index(request):
    return render(
        request, template_name="index.html",
        context={
            "key": ["a", "B", "c"],
        },
    )


@use_kwargs(
    {
        "first_name": fields.Str(missing=None),
        "last_name": fields.Str(missing=None),
        "search_text": fields.Str(missing=None),
    },
    location="query",
)
def get_all_students(request, **kwargs):


    students = Student.objects.all()
    search_fields = ["first_name", "last_name", "email", "group"]

    for parameter_name, parameter_value in kwargs.items():
        if parameter_value:
            if parameter_name == "search_text":
                or_filter = Q()
                for field in search_fields:
                    or_filter |= Q(**{f"{field}__icontains": parameter_value})
                students = students.filter(or_filter)
            else:
                students = Student.objects.filter(**{parameter_name: parameter_value})

    print(type(students))
    return render(request, template_name="students_list.html", context={"students": students})


def create_student(request):
    if request.method == "POST":
        form = StudentForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:all_students"))
    else:
        form = StudentForm()

    return render(request, template_name="students_create.html", context={"form": form})





def update_student(request, pk):
    student = get_object_or_404(Student.objects.all(), pk=pk)

    if request.method == "POST":
        form = StudentForm(request.POST, instance=student)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:all_students"))
    else:
        form = StudentForm(instance=student)

    return render(request, template_name="students_edit.html", context={"form": form})

def delete_student(request, pk):
    student = get_object_or_404(Student.objects.all(), pk=pk)
    student.delete()

    return HttpResponseRedirect(reverse("students:all_students"))

def error_404(request, exception):
    return render(request, "404.html")
