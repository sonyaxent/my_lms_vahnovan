from django.contrib import admin  # NOQA

from group.models import Group
from .models import Teacher

class TeacherAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'avatar')
    list_display_links = ('first_name',)
    search_fields = ('first_name',)
    list_filter = ('first_name', 'group')
    fields = ('first_name', 'last_name', 'group', 'rating', 'birth_date', 'avatar')
    filter_vertical = ['group']


    def save_related(self, request, form, formsets, change):
        super(TeacherAdmin, self).save_related(request, form, formsets, change)
        group = Group.objects.get(pk=1)
        form.instance.group.add(group)



admin.site.register(Teacher, TeacherAdmin)





# class TeacherAdmin(admin.ModelAdmin):
#     list_display = ('id', 'first_name', 'last_name', 'group')
#
#     def formfield_for_manytomany(self, db_field, request, **kwargs):
#         if db_field.name == "group":
#             kwargs["queryset"] = Group.objects.filter(is_active=True)
#         return super(TeacherAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)
# # Register your models here.
# admin.site.register(Teacher, TeacherAdmin)


