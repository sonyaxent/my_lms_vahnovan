from django.db import models
from faker import Faker
import datetime

from group.models import Group

class Teacher(models.Model):
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    email = models.EmailField(max_length=100, null=True)
    job_begin_date = models.DateField(null=True)
    rating = models.IntegerField(null=True)
    birth_date = models.DateField(null=True)
    avatar = models.ImageField(upload_to='photos/%Y/%m/%d', blank=True)



    group = models.ManyToManyField(to=Group, blank =True, related_name="teacher")



    def __str__(self):
        return f"{self.first_name}  {self.last_name}  {self.job_begin_date} {self.rating} {self.email} {self.group}"

    # def get_groups:
    #     pass

    def age(self):
        return datetime.datetime.now().year - self.birth_date.year

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()

        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_time_between(start_date="-50y", end_date="-24y"),
                rating=faker.random_number(fix_len=3),
                job_begin_date=faker.date_time_between(start_date="-25y", end_date="-15y"),
            )

