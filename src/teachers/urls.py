from django.urls import path
from teachers.views import get_all_teachers, create_teacher, update_teacher, delete_teacher

app_name = "teachers"

urlpatterns = [
    path("all_teachers", get_all_teachers, name="all_teachers"),
    path("create/", create_teacher, name="create_teacher"),
    path("update/<int:pk>/", update_teacher, name="update_teacher"),
    path("delete/<int:pk>/", delete_teacher, name="delete_teacher"),
]
