from django.http import HttpResponseRedirect
from django.db.models import Q
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from webargs.djangoparser import use_kwargs
from webargs import fields

from .forms import TeacherForm
from .models import Teacher


def index_teacher(request):
    return render(
        request, template_name="index_teacher.html",
        context={
            "key": ["a", "B", "c"],
        },
    )


@use_kwargs(
    {
        "first_name": fields.Str(missing=None),
        "last_name": fields.Str(missing=None),
        "search_text": fields.Str(missing=None),
    },
    location="query",
)
def get_all_teachers(request, **kwargs):


    teachers = Teacher.objects.all()
    search_fields = ["first_name", "last_name", "email", "group"]

    for parameter_name, parameter_value in kwargs.items():
        if parameter_value:
            if parameter_name == "search_text":
                or_filter = Q()
                for field in search_fields:
                    or_filter |= Q(**{f"{field}__icontains": parameter_value})
                teachers = teachers.filter(or_filter)
            else:
                teachers = Teacher.objects.filter(**{parameter_name: parameter_value})


    return render(request, template_name="teachers_list.html", context={"teachers": teachers})


def create_teacher(request):
    if request.method == "POST":
        form = TeacherForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teachers:all_teachers"))
    else:
        form = TeacherForm()

    return render(request, template_name="teachers_create.html", context={"form": form})





def update_teacher(request, pk):
    teacher = get_object_or_404(Teacher.objects.all(), pk=pk)

    if request.method == "POST":
        form = TeacherForm(request.POST, instance=teacher)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teachers:all_teachers"))
    else:
        form = TeacherForm(instance=teacher)

    return render(request, template_name="teachers_edit.html", context={"form": form})

def delete_teacher(request, pk):
    teacher = get_object_or_404(Teacher.objects.all(), pk=pk)
    teacher.delete()

    return HttpResponseRedirect(reverse("teachers:all_teachers"))

def error_404(request, exception):
    return render(request, "404.html")
